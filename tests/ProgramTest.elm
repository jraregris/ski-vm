module ProgramTest exposing (suite)

import Expect
import Test exposing (Test, describe, test)
import VM.Instruction as Instruction exposing (Instruction(..))
import VM.Program as Program
import VM.SourceCode as Source


suite : Test
suite =
    describe "Program Parser"
        [ test "Parsing multiple lines" <|
            \_ ->
                let
                    input =
                        """
                        NOP
                        """

                    expected =
                        [ NOP ]
                in
                Expect.equal expected (Program.parse (input |> Source.fromString))
        ]
