module ByteTest exposing (suite)

import Expect
import Test exposing (Test, describe, test)
import Unit.Byte as Byte


suite : Test
suite =
    let
        zero =
            Byte.fromInt 0

        one =
            Byte.fromInt 1

        two =
            Byte.fromInt 2

        twelve =
            Byte.fromInt 12

        fourteen =
            Byte.fromInt 14

        twentysix =
            Byte.fromInt 26

        hundred =
            Byte.fromInt 100

        twohundred =
            Byte.fromInt 200

        twohundredandfiftyfive =
            Byte.fromInt 255
    in
    describe "bbbbbbytes"
        [ describe "add"
            [ test "0 + 0 = 0" <|
                \_ ->
                    Expect.equal zero (Byte.add zero zero)
            , test "0 + 1 = 1" <|
                \_ ->
                    Expect.equal one (Byte.add zero one)
            , test "14 + 12 = 26" <|
                \_ ->
                    Expect.equal twentysix (Byte.add fourteen twelve)
            , test "100 + 100 = 200" <|
                \_ ->
                    Expect.equal twohundred (Byte.add hundred hundred)
            , test "255 + 0 = 255" <|
                \_ -> Expect.equal twohundredandfiftyfive (Byte.add twohundredandfiftyfive zero)
            , test "255 + 1 = 0" <|
                \_ -> Expect.equal zero (Byte.add twohundredandfiftyfive one)
            ]
        , describe "fromString"
            [ test "00" <|
                \_ ->
                    Expect.equal (Just zero) (Byte.fromString "00")
            , test "01" <|
                \_ ->
                    Expect.equal (Just one) (Byte.fromString "01")
            , test "02" <|
                \_ ->
                    Expect.equal (Just two) (Byte.fromString "02")
            , test "0c" <|
                \_ ->
                    Expect.equal (Just twelve) (Byte.fromString "0c")
            , test "ff" <|
                \_ ->
                    Expect.equal (Just twohundredandfiftyfive) (Byte.fromString "ff")
            ]
        ]
