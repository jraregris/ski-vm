module InstructionTest exposing (suite)

import Expect exposing (Expectation)
import Test exposing (..)
import Unit.Byte as Byte
import VM.Instruction as Instruction exposing (Instruction(..), Line)
import VM.Source exposing (Source(..))


suite : Test
suite =
    describe "Instruction Parser"
        [ describe "Parsing single lines" <|
            let
                zero =
                    Immediate <| Byte.fromInt 0

                one =
                    Immediate <| Byte.fromInt 1

                tests =
                    [ ( "NOP", NOP )
                    , ( "ADD 0 0", ADD zero zero )
                    , ( "ADD 1 1", ADD one one )
                    ]
            in
            tests
                |> assertParseList
        ]


assertParseList : List ( Line, Instruction ) -> List Test
assertParseList strings =
    List.map assertParse strings


assertParse : ( Line, Instruction ) -> Test
assertParse ( input, output ) =
    test (description input output) <| \_ -> Expect.equal (Instruction.parseLine input) (Just output)


description input output =
    input ++ " -> " ++ Instruction.toString output
