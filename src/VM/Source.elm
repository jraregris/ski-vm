module VM.Source exposing (Source(..), sourceToInt, sourceToString, toByte)

import Unit.Byte as Byte exposing (Byte)


type Source
    = Immediate Byte


toByte : Source -> Byte
toByte source =
    case source of
        Immediate byte ->
            byte


sourceToString : Source -> String
sourceToString source =
    case source of
        Immediate byte ->
            Byte.toString byte


sourceToInt : Source -> Int
sourceToInt source =
    case source of
        Immediate byte ->
            Byte.toInt byte
