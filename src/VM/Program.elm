module VM.Program exposing (Program, parse, toBytes, toString)

import Unit.Byte exposing (Bytes)
import VM.Instruction as Instruction exposing (Instruction(..))
import VM.SourceCode as SourceCode exposing (SourceCode)
import Values


type alias Program =
    List Instruction


parse : SourceCode -> Program
parse source =
    source
        |> SourceCode.toString
        |> String.trim
        |> String.lines
        |> List.map Instruction.parseLine
        |> Values.values


toString : Program -> String
toString program =
    program
        |> List.map Instruction.toString
        |> String.join "\n"


toBytes : Program -> Bytes
toBytes program =
    program |> List.concatMap Instruction.toBytes
