module VM.VM exposing (Msg(..), Status(..), VM, current_instruction, init, step)

import Time
import Unit.Bit as Bit
import Unit.Byte as Byte
import VM.Instruction as Instruction exposing (Instruction(..))
import VM.Memory as Memory exposing (Memory)
import VM.Program as Program
import VM.Register exposing (Flag, Register)
import VM.Source as Source
import VM.SourceCode as SourceCode exposing (SourceCode)


type alias VM =
    { source : SourceCode
    , memory : Memory
    , accumulator : Register
    , instructionPointer : Register
    , gpx : Register
    , gpy : Register
    , status : Status
    , a : Register
    , b : Register
    , halt : Flag
    , test : Flag
    }


type Status
    = Running
    | Stopped


type Msg
    = InputChanged String
    | StepButtonPressed
    | ResetButtonPressed
    | Tick Time.Posix
    | StartButtonPressed
    | StopButtonPressed


init : VM
init =
    { source = initialSource
    , memory = Memory.init |> Memory.insert (Program.parse initialSource)
    , accumulator = Byte.zero
    , instructionPointer = Byte.zero
    , gpx = Byte.zero
    , gpy = Byte.zero
    , status = Stopped
    , a = Byte.zero
    , b = Byte.zero
    , halt = Bit.Lo
    , test = Bit.Lo
    }


step : VM -> VM
step vm =
    let
        instructionSize =
            3

        instruction =
            vm.memory
                |> Memory.getBytes vm.instructionPointer instructionSize
                |> Instruction.fromBytes
    in
    vm |> applyInstruction instruction


current_instruction : VM -> Instruction
current_instruction vm =
    vm.memory
        |> Memory.getBytes vm.instructionPointer 3
        |> Instruction.fromBytes


applyInstruction : Instruction -> VM -> VM
applyInstruction ins vm =
    case ins of
        NOP ->
            { vm | instructionPointer = vm.instructionPointer |> Byte.inc 1 }

        ADD a b ->
            let
                ip =
                    vm.instructionPointer |> Byte.inc 3

                augend =
                    a |> Source.toByte

                addend =
                    b |> Source.toByte

                acc =
                    Byte.add augend addend
            in
            { vm | instructionPointer = ip, accumulator = acc }

        EOF ->
            let
                ip =
                    Byte.fromInt 0
            in
            { vm | instructionPointer = ip }

        JMP a ->
            let
                ip =
                    a
            in
            { vm | instructionPointer = ip }

        MOVL literalByte register ->
            case register of
                "GPX" ->
                    { vm | gpx = literalByte, instructionPointer = vm.instructionPointer |> Byte.inc 3 }

                "GPY" ->
                    { vm | gpy = literalByte, instructionPointer = vm.instructionPointer |> Byte.inc 3 }

                _ ->
                    vm |> Debug.log "NOT IMPLEMENTED LOL"


initialSource =
    """
Sett opp input-verdier:
MOVL 45 GPX
MOVL 73 GPY

MOV GPX A
MOV GPY B
SUB

MOV GPX A
MOV ACC B
LT

JMPT 01

MOV ACC A
MOV 0   B
EQ

JMPT FF

MOV GPX GPY
MOV ACC GPX
JMP 01

HALT

"""
        |> String.lines
        |> List.map String.trim
        |> String.join "\n"
        |> SourceCode.fromString
