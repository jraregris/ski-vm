module VM.SourceCode exposing (SourceCode, fromString, height, toString, width)


type SourceCode
    = Source String


fromString : String -> SourceCode
fromString string =
    Source string


toString : SourceCode -> String
toString source =
    case source of
        Source string ->
            string


height : SourceCode -> Int
height source =
    source
        |> toString
        |> String.lines
        |> List.length


width : SourceCode -> Int
width source =
    source
        |> toString
        |> String.lines
        |> List.map String.length
        |> List.maximum
        |> Maybe.withDefault 0
