module VM.Memory exposing (Memory, get, getBytes, init, insert)

import Array exposing (Array)
import Unit.Byte as Byte exposing (Byte, Bytes)
import VM.Program as Program exposing (Program)


memorySize =
    256


type alias Memory =
    Array Byte


init : Memory
init =
    Array.initialize memorySize (always (Byte.fromInt 0))


insert : Program -> Memory -> Memory
insert program memory =
    program
        |> Program.toBytes
        |> List.indexedMap (\i b -> ( i, b ))
        |> List.foldl (\( i, b ) m -> Array.set i b m) memory


get : Byte -> Memory -> Byte
get byte memory =
    memory |> Array.get (Byte.toInt byte) |> Maybe.withDefault (Byte.fromInt -1)


getBytes : Byte -> Int -> Memory -> Bytes
getBytes address length memory =
    let
        start =
            address |> Byte.toInt

        end =
            start + length
    in
    Array.slice start end memory |> Array.toList
