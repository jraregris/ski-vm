module VM.Register exposing (Flag, Register, flagToString, reset, toValueString)

import Unit.Bit as Bit exposing (Bit)
import Unit.Byte as Byte exposing (Byte)


type alias Register =
    Byte


type alias Flag =
    Bit


reset : Register -> Register
reset _ =
    Byte.zero


toValueString : Register -> String
toValueString =
    Byte.toString


flagToString : Flag -> String
flagToString flag =
    case flag of
        Bit.Lo ->
            "0"

        Bit.Hi ->
            "1"
