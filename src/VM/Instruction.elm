module VM.Instruction exposing (Instruction(..), Line, fromBytes, parseLine, size, toBytes, toString)

import Unit.Byte as Byte exposing (Byte, Bytes)
import VM.Source exposing (Source(..), sourceToInt, sourceToString)


type Instruction
    = ADD Source Source
    | NOP
    | EOF
    | JMP Byte
    | MOVL Byte Register


type alias Line =
    String


type alias Register =
    String


size byte =
    case byte |> Byte.toInt of
        0 ->
            1

        1 ->
            3

        2 ->
            1

        3 ->
            2

        4 ->
            3

        5 ->
            1

        6 ->
            1

        7 ->
            1

        8 ->
            1

        9 ->
            1

        10 ->
            1

        11 ->
            1

        12 ->
            1

        _ ->
            0


parseLine : Line -> Maybe Instruction
parseLine input =
    let
        tokens =
            input |> String.trim |> String.split " " |> List.map String.trim
    in
    case tokens of
        [ "NOP" ] ->
            Just NOP

        "ADD" :: a :: b :: [] ->
            case ( Byte.fromString a, Byte.fromString b ) of
                ( Just ai, Just bi ) ->
                    Just <| ADD (Immediate ai) (Immediate bi)

                _ ->
                    Nothing

        [ "EOF" ] ->
            Just EOF

        "JMP" :: a :: [] ->
            case Byte.fromString a of
                Just ai ->
                    Just <| JMP ai

                _ ->
                    Nothing

        "MOVL" :: a :: b :: [] ->
            let
                maybeValue =
                    Byte.fromString a

                maybeRegister =
                    case b of
                        "GPX" ->
                            Just "GPX"

                        "GPY" ->
                            Just "GPY"

                        _ ->
                            Nothing
            in
            Maybe.map2 (\x y -> MOVL x y) maybeValue maybeRegister

        _ ->
            Nothing


toString : Instruction -> String
toString instruction =
    case instruction of
        NOP ->
            "NOP"

        ADD a b ->
            [ "ADD", sourceToString a, sourceToString b ] |> String.join " "

        EOF ->
            "EOF"

        JMP a ->
            [ "JMP", Byte.toString a ] |> String.join " "

        MOVL byte register ->
            [ "MOVL", Byte.toString byte, register ] |> String.join " "


registerNameToInt : Register -> Int
registerNameToInt r =
    case r of
        "A" ->
            0x0A

        "GPX" ->
            0x04

        "GPY" ->
            0x05

        _ ->
            Debug.todo "make a better way to convert registers to ints"


registerNameFromInt : Int -> Register
registerNameFromInt i =
    case i of
        0x0A ->
            "A"

        0x04 ->
            "GPX"

        0x05 ->
            "GPY"

        _ ->
            "" |> Debug.log "UGH"


toBytes : Instruction -> Bytes
toBytes instruction =
    List.map Byte.fromInt <|
        case instruction of
            NOP ->
                [ 0 ]

            ADD a b ->
                [ 1, sourceToInt a, sourceToInt b ]

            EOF ->
                [ 2 ]

            JMP a ->
                [ 3, Byte.toInt a ]

            MOVL byte register ->
                [ 4, Byte.toInt byte, registerNameToInt register ]


fromBytes : Bytes -> Instruction
fromBytes bytes =
    case bytes |> List.map Byte.toInt of
        [ 0 ] ->
            NOP

        [ 0, _, _ ] ->
            NOP

        [ 1, a, b ] ->
            ADD (Immediate (Byte.fromInt <| a)) (Immediate (Byte.fromInt <| b))

        [ 2 ] ->
            EOF

        [ 2, _, _ ] ->
            EOF

        [ 3, a ] ->
            JMP (a |> Byte.fromInt)

        [ 3, a, _ ] ->
            JMP (a |> Byte.fromInt)

        [ 4, a, b ] ->
            MOVL (a |> Byte.fromInt) (b |> registerNameFromInt)

        _ ->
            NOP
