module View exposing (view)

import Array
import Element exposing (Element, fill, px)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Unit.Byte as Byte exposing (Byte)
import VM.Instruction as Instruction
import VM.Memory as Memory exposing (Memory)
import VM.Program as Program
import VM.Register as Register exposing (Flag, Register)
import VM.SourceCode as SourceCode exposing (SourceCode)
import VM.VM as VM exposing (Msg(..), Status(..), VM)


color1 =
    Element.rgb255 0x33 0x1E 0x50


color2 =
    Element.rgb255 0xA6 0x37 0x25


color3 =
    Element.rgb255 0xD6 0x8E 0x49


color4 =
    Element.rgb255 0xF7 0xE7 0xC6


view : VM -> Html Msg
view vm =
    Element.layout
        [ Font.family [ Font.typeface "Fira Code", Font.monospace ]
        , Font.medium
        , Font.variant Font.slashedZero
        , Background.color color4
        , Font.color color1
        , Element.width fill
        ]
        (Element.column
            [ Element.width fill
            ]
            [ Element.row
                [ Element.centerX ]
                [ Element.el
                    [ Element.centerX, Region.heading 1, Font.size 30 ]
                    (Element.text "Ski VM")
                , controlView vm
                ]
            , Element.wrappedRow
                [ Element.padding 20
                , Element.spacing 20
                ]
                [ memoryView vm.memory
                    vm.instructionPointer
                , registerView vm
                , instructionView vm
                ]
            , Element.wrappedRow []
                [ programView vm.source
                , sourceView vm.source
                ]
            , descriptionView
            ]
        )


controlView vm =
    Element.wrappedRow
        [ Element.padding 20
        , Element.spacing 20
        , Element.width fill
        ]
        [ resetView, stepView, startStopView vm.status ]


descriptionView =
    Element.el
        [ Element.padding 20
        , Element.spacing 20
        ]
        (Element.el
            [ Background.color color4
            , Font.color color3
            , Element.padding 20
            ]
            (Element.text ("""
== INSTRUCTIONS =========================================

00 -- -- <- NOP         -->  No operation
01 xx yy <- ADD x y     -->  Add x and y, put sum on ACC
02 -- -- <- EOF         -->  Reset instruction pointer
03 xx -- <- JMP x       -->  Set instruction pointer to x
04 xx yy <- MOVL x y    -->  Set register y to literal x

== REGISTERS ============================================

00 <- INS
01 <- ACC
02 <- A
03 <- B
04 <- GPX
05 <- GPY

    """ |> String.trim))
        )


resetView =
    Input.button
        [ Background.color color2
        , Font.color color4
        , Element.padding 23
        , Element.alignRight
        ]
        { onPress = Just ResetButtonPressed
        , label = Element.text "Reset"
        }


stepView =
    Input.button
        [ Background.color color1
        , Font.color color4
        , Element.padding 23
        , Element.alignRight
        ]
        { onPress = Just StepButtonPressed
        , label = Element.text "Step"
        }


startStopView status =
    let
        ( msg, label ) =
            case status of
                Running ->
                    ( Just StopButtonPressed, "Stop!" )

                Stopped ->
                    ( Just StartButtonPressed, "Start" )
    in
    Input.button
        [ Background.color color1
        , Font.color color4
        , Element.padding 23
        , Element.alignRight
        ]
        { onPress = msg
        , label = Element.text label
        }


instructionView : VM -> Element msg
instructionView vm =
    Element.column [ Element.height fill, Element.width fill ]
        [ h2 "Next instruction"
        , vm
            |> VM.current_instruction
            |> Instruction.toString
            |> Element.text
            |> Element.el [ Element.alignTop ]
        ]


memoryView : Memory -> Byte -> Element msg
memoryView mem ip =
    Element.column
        [ Element.height fill ]
        [ h2 "Memory"
        , renderMemory mem ip
        ]


registerView : VM -> Element msg
registerView vm =
    Element.column [ Element.height fill ]
        [ h2 "Registers"
        , Element.column
            [ Element.alignTop ]
            [ renderFlag "HALT" vm.halt
            , renderRegister "INS " vm.instructionPointer
            , renderRegister "ACC " vm.accumulator
            , renderRegister "A   " vm.a
            , renderRegister "B   " vm.b
            , renderFlag "TEST" vm.test
            , renderRegister "GPX " vm.gpx
            , renderRegister "GPY " vm.gpy
            ]
        ]


renderRegister : String -> Register -> Element msg
renderRegister name register =
    Element.text
        (name ++ ": " ++ (register |> Register.toValueString))


renderFlag : String -> Flag -> Element msg
renderFlag name flag =
    Element.text
        (name ++ ": " ++ (flag |> Register.flagToString))


programView : SourceCode -> Element msg
programView input =
    Element.column
        [ Element.width (Element.fillPortion 1)
        , Element.height fill
        , Element.padding 20
        , Element.spacing 20
        ]
        [ h2 "Listing"
        , Element.text (input |> Program.parse |> Program.toString)
        ]


sourceView : SourceCode -> Element Msg
sourceView source =
    Element.column
        [ Element.width (Element.minimum 50 (px 400))
        , Element.height fill
        , Element.padding 20
        , Element.spacing 20
        ]
        [ h2 "Input"
        , Input.multiline
            [ Element.htmlAttribute
                (Html.Attributes.cols (max 3 (SourceCode.width source)))
            , Element.htmlAttribute
                (Html.Attributes.rows (SourceCode.height source))
            , Font.color color1
            , Background.color color4
            , Border.color color1
            ]
            { onChange = InputChanged
            , text = source |> SourceCode.toString
            , placeholder = Nothing
            , label = Input.labelHidden "input"
            , spellcheck = False
            }
        ]


h2 text =
    Element.paragraph
        [ Region.heading 2
        , Font.size 20
        , Font.color color2
        ]
        [ Element.text text ]


renderMemory : Memory -> Byte -> Element msg
renderMemory mem ip =
    let
        instructionSize =
            Instruction.size (mem |> Memory.get ip)
    in
    Element.column []
        (mem
            |> Array.map Byte.toString
            |> Array.indexedMap
                (\i a ->
                    Element.el
                        (byteStyle i ip instructionSize)
                        (Element.text a)
                )
            |> Array.toList
            |> split 16
            |> memoryLinesView
        )


byteStyle index instructionPointer length =
    let
        start =
            instructionPointer |> Byte.toInt

        end =
            start + length

        highlighted =
            index >= start && index < end

        left =
            index == start

        right =
            index == end

        center =
            highlighted == True && left == False && right == False
    in
    if highlighted then
        [ Font.color color4, Background.color color1 ]

    else
        [ Font.color color1 ]


memoryLinesView =
    List.indexedMap
        (\i a ->
            Element.row
                [ Element.spacing 10 ]
                (address i :: a)
        )


address i =
    Element.el
        [ Font.size 12
        , Element.centerY
        ]
        (Element.text (Byte.fromInt (i * 16) |> Byte.toString))


split : Int -> List a -> List (List a)
split i list =
    case List.take i list of
        [] ->
            []

        listHead ->
            listHead :: split i (List.drop i list)
