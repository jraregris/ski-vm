module Main exposing (main)

import Browser
import Time
import Update
import VM.VM as VM exposing (Msg(..), Status(..), VM)
import View


main : Program () VM Msg
main =
    Browser.element
        { init = \_ -> init
        , subscriptions = subs
        , update = Update.update
        , view = View.view
        }


init : ( VM, Cmd Msg )
init =
    ( VM.init, Cmd.none )


subs : VM -> Sub Msg
subs vm =
    case vm.status of
        Running ->
            Time.every 100 Tick

        Stopped ->
            Sub.none
