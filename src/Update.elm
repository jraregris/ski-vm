module Update exposing (update)

import VM.Memory as Memory
import VM.Program as Program
import VM.Register as Register
import VM.SourceCode as Source
import VM.VM as VM exposing (Msg(..), Status(..), VM)


update : Msg -> VM -> ( VM, Cmd Msg )
update msg vm =
    case msg of
        InputChanged input ->
            let
                source =
                    input |> Source.fromString

                memory =
                    Memory.init |> Memory.insert (source |> Program.parse)
            in
            ( { vm | source = source, memory = memory }, Cmd.none )

        ResetButtonPressed ->
            let
                ip =
                    vm.instructionPointer |> Register.reset

                acc =
                    vm.accumulator |> Register.reset
            in
            ( { vm | instructionPointer = ip, accumulator = acc }, Cmd.none )

        StepButtonPressed ->
            ( vm |> VM.step, Cmd.none )

        Tick _ ->
            update StepButtonPressed vm

        StartButtonPressed ->
            ( { vm | status = Running }, Cmd.none )

        StopButtonPressed ->
            ( { vm | status = Stopped }, Cmd.none )
