module Unit.Bit exposing (Bit(..), add, addWithCarry, fromInt, highpass)

import Bitwise


type Bit
    = Hi
    | Lo


fromInt : Int -> Int -> Bit
fromInt value input =
    if (input |> Bitwise.and value) == value then
        Hi

    else
        Lo


highpass : number -> Bit -> number
highpass i bit =
    case bit of
        Hi ->
            i

        Lo ->
            0


add : Bit -> Bit -> ( Bit, Carry )
add a b =
    case ( a, b ) of
        ( Lo, Lo ) ->
            ( Lo, Lo )

        ( Lo, Hi ) ->
            ( Hi, Lo )

        ( Hi, Lo ) ->
            ( Hi, Lo )

        ( Hi, Hi ) ->
            ( Lo, Hi )


addWithCarry : Bit -> Bit -> Carry -> ( Bit, Carry )
addWithCarry a b c =
    case ( a, b, c ) of
        ( Lo, Lo, Lo ) ->
            ( Lo, Lo )

        ( Lo, Lo, Hi ) ->
            ( Hi, Lo )

        ( Lo, Hi, Lo ) ->
            ( Hi, Lo )

        ( Lo, Hi, Hi ) ->
            ( Lo, Hi )

        ( Hi, Lo, Lo ) ->
            ( Hi, Lo )

        ( Hi, Lo, Hi ) ->
            ( Lo, Hi )

        ( Hi, Hi, Lo ) ->
            ( Lo, Hi )

        ( Hi, Hi, Hi ) ->
            ( Hi, Hi )


type alias Carry =
    Bit
