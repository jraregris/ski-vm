module Unit.Nibble exposing (Nibble, fromChar, toString)

import Unit.Bit exposing (Bit(..))


type alias Nibble =
    { a : Bit
    , b : Bit
    , c : Bit
    , d : Bit
    }


toString : Nibble -> String
toString { a, b, c, d } =
    case a of
        Hi ->
            case b of
                Hi ->
                    case c of
                        Hi ->
                            case d of
                                Hi ->
                                    "f"

                                Lo ->
                                    "e"

                        Lo ->
                            case d of
                                Hi ->
                                    "d"

                                Lo ->
                                    "c"

                Lo ->
                    case c of
                        Hi ->
                            case d of
                                Hi ->
                                    "b"

                                Lo ->
                                    "a"

                        Lo ->
                            case d of
                                Hi ->
                                    "9"

                                Lo ->
                                    "8"

        Lo ->
            case b of
                Hi ->
                    case c of
                        Hi ->
                            case d of
                                Hi ->
                                    "7"

                                Lo ->
                                    "6"

                        Lo ->
                            case d of
                                Hi ->
                                    "5"

                                Lo ->
                                    "4"

                Lo ->
                    case c of
                        Hi ->
                            case d of
                                Hi ->
                                    "3"

                                Lo ->
                                    "2"

                        Lo ->
                            case d of
                                Hi ->
                                    "1"

                                Lo ->
                                    "0"


fromChar : Char -> Maybe Nibble
fromChar char =
    case char of
        '0' ->
            Just <| Nibble Lo Lo Lo Lo

        '1' ->
            Just <| Nibble Lo Lo Lo Hi

        '2' ->
            Just <| Nibble Lo Lo Hi Lo

        '3' ->
            Just <| Nibble Lo Lo Hi Hi

        '4' ->
            Just <| Nibble Lo Hi Lo Lo

        '5' ->
            Just <| Nibble Lo Hi Lo Hi

        '6' ->
            Just <| Nibble Lo Hi Hi Lo

        '7' ->
            Just <| Nibble Lo Hi Hi Hi

        '8' ->
            Just <| Nibble Hi Lo Lo Lo

        '9' ->
            Just <| Nibble Hi Lo Lo Hi

        'a' ->
            Just <| Nibble Hi Lo Hi Lo

        'b' ->
            Just <| Nibble Hi Lo Hi Hi

        'c' ->
            Just <| Nibble Hi Hi Lo Lo

        'd' ->
            Just <| Nibble Hi Hi Lo Hi

        'e' ->
            Just <| Nibble Hi Hi Hi Lo

        'f' ->
            Just <| Nibble Hi Hi Hi Hi

        _ ->
            Nothing
