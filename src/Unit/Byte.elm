module Unit.Byte exposing (Byte, Bytes, add, fromInt, fromString, inc, toInt, toString, zero)

import Unit.Bit as Bit exposing (Bit(..), highpass)
import Unit.Nibble as Nibble exposing (Nibble)


{-| A big endian 8 bit byte. a is bigger than h.
-}
type alias Byte =
    { a : Bit
    , b : Bit
    , c : Bit
    , d : Bit
    , e : Bit
    , f : Bit
    , g : Bit
    , h : Bit
    }


toInt : Byte -> Int
toInt { a, b, c, d, e, f, g, h } =
    let
        ai =
            a |> highpass 128

        bi =
            b |> highpass 64

        ci =
            c |> highpass 32

        di =
            d |> highpass 16

        ei =
            e |> highpass 8

        fi =
            f |> highpass 4

        gi =
            g |> highpass 2

        hi =
            h |> highpass 1
    in
    ai + bi + ci + di + ei + fi + gi + hi


fromInt : Int -> Byte
fromInt int =
    let
        a =
            int |> Bit.fromInt 128

        b =
            int |> Bit.fromInt 64

        c =
            int |> Bit.fromInt 32

        d =
            int |> Bit.fromInt 16

        e =
            int |> Bit.fromInt 8

        f =
            int |> Bit.fromInt 4

        g =
            int |> Bit.fromInt 2

        h =
            int |> Bit.fromInt 1
    in
    Byte a b c d e f g h


type alias Bytes =
    List Byte


zero : Byte
zero =
    Byte Lo Lo Lo Lo Lo Lo Lo Lo


toString : Byte -> String
toString { a, b, c, d, e, f, g, h } =
    let
        high =
            Nibble a b c d |> Nibble.toString

        low =
            Nibble e f g h |> Nibble.toString
    in
    high ++ low


inc : Int -> Byte -> Byte
inc amount byte =
    amount |> fromInt |> add byte


add : Byte -> Byte -> Byte
add a b =
    let
        ( hh, hhc ) =
            Bit.add a.h b.h

        ( gg, ggc ) =
            Bit.addWithCarry a.g b.g hhc

        ( ff, ffc ) =
            Bit.addWithCarry a.f b.f ggc

        ( ee, eec ) =
            Bit.addWithCarry a.e b.e ffc

        ( dd, ddc ) =
            Bit.addWithCarry a.d b.d eec

        ( cc, ccc ) =
            Bit.addWithCarry a.c b.c ddc

        ( bb, bbc ) =
            Bit.addWithCarry a.b b.b ccc

        ( aa, _ ) =
            Bit.addWithCarry a.a b.a bbc
    in
    Byte aa bb cc dd ee ff gg hh


fromNibbles : Nibble -> Nibble -> Byte
fromNibbles left right =
    Byte left.a left.b left.c left.d right.a right.b right.c right.d


fromString : String -> Maybe Byte
fromString string =
    case String.toList string of
        [ a ] ->
            case Nibble.fromChar a of
                Just nibble ->
                    Just (fromNibbles (Nibble Lo Lo Lo Lo) nibble)

                Nothing ->
                    Nothing

        [ a, b ] ->
            let
                left =
                    Nibble.fromChar a

                right =
                    Nibble.fromChar b
            in
            Maybe.map2 fromNibbles left right

        _ ->
            Nothing
